#r "packages/WindowsAzure.ServiceBus/lib/net45-full/Microsoft.ServiceBus.dll"
#r "System.Runtime.Serialization"
open Microsoft.ServiceBus.Messaging
open FSharp.Data

#load "constants.fsx"

let factory() = sprintf "Endpoint=%s;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=%s" Constants.sbEndpoint Constants.sbRootKey
                |> MessagingFactory.CreateFromConnectionString
    
let send url (body: string) =
    let snd = factory().CreateMessageSender url
    use bodyStream = new System.IO.MemoryStream( body |> System.Text.Encoding.UTF8.GetBytes )
    use msg = new BrokeredMessage( bodyStream )
    snd.Send msg

let indexRecord typ queue id = send queue (sprintf """{ "Index": "%s", "DocumentID": %d }""" typ id)

let getBatch conn table count highwater =
    use cmd = (conn: System.Data.IDbConnection).CreateCommand()
    cmd.CommandText <- sprintf "select top %d [%s_id] from [%s] where [%s_id] > %d order by [%s_id]" count table table table highwater table
    use rd = cmd.ExecuteReader()
    [ while rd.Read() do
        yield rd.GetInt32 0 ]

let rec loop getBatch indexRecord highwater =
    System.IO.File.WriteAllText (__SOURCE_DIRECTORY__ + "/last-highwater", string highwater)
    let ids = getBatch highwater
    if List.isEmpty ids then highwater
    else
        for id in ids do
            printfn "Enqueuing %d (Ctrl+C to stop)" id
            indexRecord id

        loop getBatch indexRecord (ids |> List.max)

let run highwater =
    use conn = new System.Data.SqlClient.SqlConnection( Constants.dbConnStr )
    conn.Open()
    loop (getBatch conn Constants.dbTable Constants.idChunk) (indexRecord Constants.dbTable Constants.sbQueue) highwater

let args = System.Environment.GetCommandLineArgs()
if args.Length < 3 then
    printfn "Expected one argument - ID high water."
else
    let newHw = run (args.[2] |> int)
    printfn "New highwater ID is %d" newHw